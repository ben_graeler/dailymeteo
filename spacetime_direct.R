## spacetime direct
library(gstat)
library(rgdal)


load(url("http://biogeo.ucdavis.edu/data/gadm2/R/HRV_adm0.RData"))

tempST@sp <- spTransform(tempST@sp, CRS(proj4string(LST)))
gadm <- spTransform(gadm, CRS(proj4string(LST)))

pdf("TeX/figures/HRspatio-temporalseries.pdf",width=6, height=4)
stplot(as(tempST[,111:120,"TEMP"], "STFDF"), col.regions=bpy.colors(), 
       sp.layout = list("sp.polygons", gadm, first=F),
       cuts=0:10*2, main="", key.space="right")
dev.off()

vgmMeasure <- variogram(TEMP~1, tempST, tlags=0:7, cutoff=150)

pdf("TeX/figures/empStVgm_wf.pdf",width=6, height=5)
plot(vgmMeasure, wireframe=T, scales=list(arrows=F),
     col.regions=bpy.colors(),zlab=list(rot=90),zlim=c(0,16))
dev.off()

pdf("TeX/figures/empStVgm.pdf",width=6, height=5)
plot(vgmMeasure)
dev.off()


# metric model
metricModel <- vgmST("metric",
                     joint=vgm(12, "Exp", 150, 2, kappa=0.6),
                     stAni=50)
modTempMetric <- fit.StVariogram(vgmMeasure, metricModel)
attr(modTempMetric, "optim.out")$value # Exp: 1.48, Sph: 1.58, Mat,3: 1.54

pdf("TeX/figures/metricStVgm.pdf",width=6, height=4)
plot(vgmMeasure, modTempMetric)
dev.off()
plot(vgmMeasure, modTempMetric, wireframe=T, all=T, scales=list(arrows=F))

##
# separable model: spatial and temporal sill will be ignored
# and kept constant at 1-nugget respectively. A joint sill is used.
separableModel <- vgmST("separable", 
                        space=vgm(0.9,"Exp", 147, 0.1),
                        time =vgm(0.9,"Exp", 3.5, 0.1),
                        sill=14)

modTempSep <- fit.StVariogram(vgmMeasure, separableModel, lower=c(10,0,1,0,0))
attr(modTempSep, "optim.out")$value 
# Exp-Exp: 0.78
# Exp-Sph: 0.83
# Sph-Sph: 0.83
# Sph-Exp: 0.80

pdf("TeX/figures/sepStVgm.pdf",width=6, height=4)
plot(vgmMeasure, modTempSep)
dev.off()
plot(vgmMeasure, modTempSep, wireframe=T, all=T, scales=list(arrows=F))

# product sum model: spatial and temporal nugget will be ignored and kept
# constant at 0. Only a joint nugget is used.
prodSumModel <- vgmST("productSum",
                      space=vgm(10, "Sph", 150, 0),
                      time= vgm(10, "Exp",   3, 0), 
                      sill=14, nugget=2)
modTempProdSum <- fit.StVariogram(vgmMeasure, prodSumModel, lower=c(0,10,0,1,10,0))
attr(modTempProdSum, "optim.out")$value 
# Exp-Exp: 0.73
# Exp-Sph: 0.78
# Sph-Sph: 0.77
# Sph-Exp: 0.71

pdf("TeX/figures/psStVgm.pdf",width=6, height=4)
plot(vgmMeasure, modTempProdSum)
dev.off()

plot(vgmMeasure, modTempProdSum, wireframe=T, all=T, scales=list(arrows=F))

# sum metric model: spatial, temporal and joint nugget will be estimated
sumMetricModel <- vgmST("sumMetric",
                        space=vgm( 5, "Exp", 100, 1),
                        time =vgm( 5, "Mat",   5, 1, kappa=1),
                        joint=vgm( 5, "Exp", 100, 0),
                        stAni=20)
modTempSumMetric <- fit.StVariogram(vgmMeasure, sumMetricModel, lower=c(0,10,0,
                                                                        0, 1,0,
                                                                        0,10,0,
                                                                        5))
attr(modTempSumMetric, "optim.out")$value 
# Exp-Exp-Exp: 0.62
# Exp-Sph-Sph: 0.508
# Sph-Sph-Sph: 0.515
# Exp-Sph-Exp: 0.507
# Exp-Mat-Exp: 0.482, kappa=3
# Exp-Mat-Exp: 0.437, kappa=1
# Exp-Mat-Sph: 0.437, kappa=1

pdf("TeX/figures/sumMetricStVgm.pdf",width=6, height=4)
plot(vgmMeasure, modTempSumMetric)
dev.off()

plot(vgmMeasure, modTempSumMetric, wireframe=T, all=T, scales=list(arrows=F))

plot(vgmMeasure, list(modTempMetric, modTempSep, modTempProdSum, modTempSumMetric), all=T)

pdf("TeX/figures/allStVgm.pdf",width=6, height=4)
plot(vgmMeasure, list(modTempMetric, modTempSep, modTempProdSum, modTempSumMetric),
     wireframe=T, all=T, xlab="", ylab="", zlab="")
dev.off()

## kriging
tempST <- as(tempST, "STSDF")

LST <- as(LST, "SpatialPixelsDataFrame")
proj4string(tempST@sp) <- CRS(proj4string(LST))

targetSTF <- STF(geometry(LST), tempST[,115,drop=F]@time, tempST[,115,drop=F]@endTime)
proj4string(targetSTF@sp) <- CRS(proj4string(LST))

# metric
IstriaKrigeMetric <- krigeST(TEMP~1, tempST[,110:120],
                             targetSTF,
                             model=modTempMetric)

pdf("TeX/figures/krigeMetric.pdf",width=6, height=4)
spplot(IstriaKrigeMetric[,1], "var1.pred",col.regions=bpy.colors(),
       sp.layout = list("sp.polygons", gadm, first=F), main="metric")
dev.off()


# separable
IstriaKrigeSep <- krigeST(TEMP~1, tempST[,110:120],
                             targetSTF,
                             model=modTempSep)

pdf("TeX/figures/krigeSeparable.pdf",width=6, height=4)
spplot(IstriaKrigeSep[,1], "var1.pred",col.regions=bpy.colors(), 
       sp.layout = list("sp.polygons", gadm, first=F), main="separable")
dev.off()

# product-sum
IstriaKrigeProdSum <- krigeST(TEMP~1, tempST[,110:120],
                          targetSTF,
                          model=modTempProdSum)

pdf("TeX/figures/krigeProdSum.pdf",width=6, height=4)
spplot(IstriaKrigeProdSum[,1], "var1.pred",col.regions=bpy.colors(), 
       sp.layout = list("sp.polygons", gadm, first=F), main="product-sum")
dev.off()

# sum-metric
IstriaKrigeSumMetric <- krigeST(TEMP~1, tempST[,110:120],
                                targetSTF,
                                model=modTempSumMetric)

pdf("TeX/figures/krigeSumMetric.pdf",width=6, height=4)
spplot(IstriaKrigeSumMetric[,1], "var1.pred",col.regions=bpy.colors(), 
       sp.layout = list("sp.polygons", gadm, first=F), main="sum-metric")
dev.off()

# series

tempST@sp <- spTransform(tempST@sp, CRS(proj4string(HRgrid1km)))
projVariogram <- variogram(TEMP~1, tempST, tlags=0:7)
projSumMetricModel <- vgmST("sumMetric",
                            space=vgm( 5, "Exp", 100000, 1),
                            time =vgm( 5, "Mat",   5, 1, kappa=1),
                            joint=vgm( 5, "Exp", 100000, 0),
                            stAni=20)
projModTempSumMetric <- fit.StVariogram(projVariogram, projSumMetricModel,
                                       lower=c(0,10,0,
                                               0, 1,0,
                                               0,10,0,
                                               5))
attr(projModTempSumMetric, "optim.out")$value 
plot(projVariogram, projModTempSumMetric)

targetSTFseries <- STF(geometry(LST), tempST[,111:120,drop=F]@time, tempST[,111:120,drop=F]@endTime)
proj4string(targetSTFseries@sp) <- CRS(proj4string(HRgrid1km))

IstriaSeriesKrigeSumMetric <- krigeST(TEMP~1, tempST[,108:122], nmax=10,
                                      targetSTFseries,
                                      model=projModTempSumMetric)

pdf("TeX/figures/predSt.pdf",width=6, height=4)
stplot(IstriaSeriesKrigeSumMetric[, "var1.pred"], col.regions=bpy.colors(), 
       sp.layout = list("sp.polygons", gadm, first=F), main="interpolated Time Series")
dev.off()
