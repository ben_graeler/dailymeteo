\documentclass[compress,mathserif,table,href]{beamer}

\usepackage{animate}
\usepackage[numbers]{natbib}
\newcommand{\newblock}{}

\input{lecture-style.tex}

\graphicspath{{figures/}}

\lecture[1]{Spatio-Temporal Kriging}

\date{2014-06-24}
\rowcolors{1}{}{}
\begin{document}

\begin{frame}
\maketitle
\end{frame}		

\section{Introduction}

\begin{frame}{Spatial Data}
From a purely statistical perspective, spatial data is multivariate data with special covariates: the coordinates. \vspace{4pt}

Tobler's first law of Geography states \citep{Tobler1970}: 

\begin{quote}
Everything is related to everything else, but near things are more related than distant things.
\end{quote}
\end{frame}

\begin{frame}{Coordinate Reference System}

We model the earth, but think in maps: locations are projected from a curved surface in 3D to flat 2D space.\vspace{4pt}

Be aware of geographic coordinates and different projections that maintain angles, certain distances or area.\vspace{4pt}

Imagine the following distances between:\\
\begin{itemize}
\item the Fjord of Oslo (59.85 N 10.75 E) and Uppsala (59.85~N 17.63~E) that are at the same latitude:
\begin{description}
\item[Degrees:] 6.88
\item[Great Circle:] 385~km
\item[Rate:] 56 km/degree
\end{description}
\item the intersections of the Congo river with the equator (0.00~N 18.21~E) and (0.00~N, 25.53~E):
\begin{description}
\item[Degrees:] 7.32	
\item[Great Circle:] 814~km
\item[Rate:] 111~km/degree
\end{description}
\end{itemize}
\end{frame}

\begin{frame}{CRS identifier}
To distinguish different projections, a well prepared data set comes with its coordinate reference system (CRS) as metadata. \vspace{4pt}

These are often encoded as
\begin{itemize}
\item<1-> EPSG-codes  (by the  European Petroleum Survey Group)
\item<1-> proj4string
\end{itemize}

They define how the reference surface (sphere, ellipsoid) is fixed to the real world (called the datum) and how the projection (surface in 3D to 2D plane) is made.
\end{frame}

\begin{frame}{Projection}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{projection}
\end{figure}
\end{frame}

\begin{frame}{Fields}
\emph{Fields} are understood as continuously spreading over space and/or time (e.g. temperature recordings) and typically observed at a set of distinct locations for a series of time steps. Fields are typically illustrated as interpolated maps and modelled as a realisation of a spatial/spatio-temporal random field.

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{HRObsIDW}
\end{figure}
\end{frame}

\begin{frame}{Stationarity and Isotropy}
\begin{description}
\item<1->[stationarity] The process "looks" the same at each location (e.g. mean and variance do not change from east to west)
\item<1->[isotropy] The dependence between locations is determined only by their separating distance neglecting the direction (e.g. locations 2~km apart along the north-south axis are as correlated as stations 2~km apart along the east-west axis)
\end{description}

Some tricks exist to weaken these assumptions (e.g. rotating and rescaling coordinates).
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Variograms}
The dependence across space of a random field $Z$ is assessed using a \emph{variogram} $\gamma$:
$$ \gamma(h) = \frac{1}{2}\rm{E}\big(Z(s) - Z(s+h)\big)^2$$

the empirical estimator looks like 
$$ \hat{\gamma}(h) = \frac{1}{2 |N_h|} \sum\limits_{(i,j)\in N_h}\big(Z(s_i)-Z(s_j)\big)^2$$
while $N_h = \{(i,j) : h-\epsilon \leq ||s_i-s_j|| \leq h+\epsilon \}$

\framebreak
The \emph{sample variogram} is obtained through
\footnotesize
\begin{verbatim}
vgmTempDay <- variogram(TEMP~1, tempDay)
\end{verbatim}
\normalsize

\begin{figure}
\includegraphics[width=\textwidth]{HRVgm}
\end{figure}

\framebreak
And a theoretical \emph{variogram model} can be fitted

\footnotesize
\begin{verbatim}
> head(vgm())
  short                        long
1   Nug                Nug (nugget)
2   Exp           Exp (exponential)
3   Sph             Sph (spherical)
4   Gau              Gau (gaussian)
5   Exc Exclass (Exponential class)
6   Mat                Mat (Matern)

> vgmSphHR <- fit.variogram(vgmTempDay,
                            vgm(10,"Sph", 120, 4))
> vgmSphHR
  model    psill    range
1   Nug 3.338608  0.00000
2   Sph 7.737535 98.48023
\end{verbatim}
\normalsize

\begin{figure}
\includegraphics[width=\textwidth]{HRVgmModel}
\end{figure}
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Kriging}
Certain variogram models can be used to parametrize a covariance matrix for a Gaussian random field over a finite set of locations $s_1$, \dots, $s_n$:
$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu}, \Sigma\big) $$
while $\Sigma = (\sigma^2_{ij})_{ij}$ and $\sigma^2_{ij}=\sigma^2 - \gamma(||s_i-s_j||)$, $1 \leq i,j\leq n$ with $\sigma^2=\rm{Var}\big(Z(s)\big)$, $\boldsymbol{\mu}=(\mu_1,\dots,\mu_n)$.\vspace{4pt}

Predictions can be made using matrix inversion and matrix multiplications.

\framebreak
\footnotesize
\begin{verbatim}
krige(TEMP~1, tempDay, geometry(LST), model=vgmSphHR)
\end{verbatim}
\normalsize

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{HRObsKrige}
\end{figure}

\framebreak

The model quantifies how \emph{uncertain} it is about the estimates through the kriging variance:

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{HRKrigeVar}
\end{figure}
\end{frame}

\begin{frame}{Overview of kriging types}
\begin{description}
\item[simple kriging] the mean value is known
\item[ordinary kriging] prediction based on coordinates 
\item[universal kriging] prediction based on coordinates and additional regressors (elevation)
\item[co-kriging] the cross-variogram between two variables is as well exploit (solar radiation)
\end{description}
\end{frame}

\section{Spatio-Temporal Kriging}

\begin{frame}{Spatio-Temporal Data}
$S \times T$ works as a data structure, but modelling needs to consider special properties of the product of space and time.
\begin{description}
\item<1->[direction] Today's values influence tomorrow, but will not take effect on yesterday's values.
\item<1->[anisotropy] What is the equivalent in terms of dependence of 1~m separation in seconds or minutes?
\end{description}

The easiest way to think of spatio-temporal data is as time slices - but this neglects the temporal dependence.\vspace{4pt}

After modelling temporal trend or periodicities, the residuals might be modelled as a spatio-temporal random field.
\end{frame}

\begin{frame}{Almost Spatio-Temporal approaches}
\begin{description}
\item[slice wise] the easiest adoption is to do interpolation per slice fitting a variogram model for each time slice
\item[pooled] the variogram is fitted based on all spatio-temporal data and is used to predict each time slice separately with the same model
\item[evolving] models mix the both extremes such that the variogram model adopts to the daily situation (e.g. in terms of overall variability, the sill) but range and the nugget/sill ratio depend on larger data samples.
\end{description}
\end{frame}

\begin{frame}
\frametitle{The spatio-temporal variogram}
Extending the variogram to a twoplace function for spatio-temporal random fields $Z(s,t)$:
$$\gamma(h,u) = \rm{E}\big(Z(s,t)-Z(s+h,t+u) \big)^2$$
at any location $(s,t)$ and empirical version
$$ \hat{\gamma}(h,u) = \frac{1}{2 |N_{h,u}|} \sum\limits_{(i,j)\in N_{h,u}}\big(Z(s_i,t_i)-Z(s_j,t_j)\big)^2$$
while $N_{h,u} = \left\{(i,j) \left|\begin{array}{l}
h-\epsilon_s \leq ||s_i-s_j|| \leq h+\epsilon_s \\
u-\epsilon_t \leq t_i-t_j \leq u+\epsilon_t 
\end{array} \right.\right \}$
\end{frame}

\begin{frame}{Scenario}
%We have a set of spatially spread time series of daily measurements and are asked to produce a map of means for the provided time frame. The data is provided by the EEA, the presentation is composed along the lines of \cite{Graler2012}.

\begin{figure}
\includegraphics[width=\textwidth]{HRspatio-temporalseries.pdf}
\end{figure}
\end{frame}


\subsection{empirical}

\begin{frame}[fragile]
\frametitle{empirical spatio-temporal variogram surface}
The idea is the same as in the spatial case: binning of locations according to their separating distance. In the spatio-temporal case, distances are pairs of spatial and temporal distance yielding a variogram surface, not a single line.

\footnotesize
\begin{verbatim}
vgmMeasure <- variogram(TEMP~1, tempST,
                        tlags=0:7, cutoff=150)

# wireframe:
plot(vgmMeasure, wireframe=T, scales=list(arrows=F),
     col.regions=bpy.colors(),zlab=list(rot=90),zlim=c(0,16))

# levelplot:
plot(vgmMeasure)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{empirical spatio-temporal variogram surface - wireframe}
\begin{figure}
\includegraphics[width=0.85\textwidth]{empStVgm_wf.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{empirical spatio-temporal variogram surface - levelplot}
\begin{figure}
\includegraphics[width=0.85\textwidth]{empStVgm.pdf}
\end{figure}
\end{frame}

\subsection{metric}

\begin{frame}
\frametitle{metric kriging}
The \alert{metric kriging} follows the natural idea of extending the 2-dimensional geographic space into a 3-dimensional spatio-temporal one. In order to achieve an isotropic space, the temporal domain has to be rescaled to match the spatial one (spatio-temporal anisotropy correction $\kappa$). \linebreak

All spatial, temporal and sptio-temporal distances are treated equally resulting in a joint covariance model $C_j$: 
$$C_m(h,u)=C_j\big(\sqrt{h^2+(\kappa\cdot u)^2}\big)$$
The variogram evaluates to
$$ \gamma_m(h,u) = \gamma_j(\sqrt{h^2+(\kappa\cdot u)^2}) $$
where $\gamma_j$ is any known variogram including some nugget effect.
\end{frame}


\begin{frame}[fragile]
\frametitle{metric kriging in R}

\footnotesize
\begin{verbatim}
metricModel <- vgmST("metric",
                     joint=vgm(12, "Exp", 150, 2),
                     stAni=50)
modTempMetric <- fit.StVariogram(vgmMeasure, metricModel)
attr(modTempMetric, "optim.out")$value # 1.48

plot(vgmMeasure, modTempMetric)

IstriaKrigeMetric <- krigeST(TEMP~1, tempST[,110:120],
                             targetSTF,
                             model=modTempMetric)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{metric spatio-temporal variogram surface}
\begin{figure}
\includegraphics[width=0.85\textwidth]{metricStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 115 - metric model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{krigeMetric.pdf}
\end{figure}
\end{frame}


\subsection{separable}

\begin{frame}{separable covariance models}
In space and under the assumptions of isotropy and stationarity, the covariance is a function $C(h)$
of the separating distance $h$ between two locations. A spatio-temporal covariance function is thought of as a function of a spatial and a temporal distance $C(h,t)$. \linebreak 

A \alert{separable covariance function} is assumed to fulfill  $C_{sep}(h,u)=C_s(h)C_t(u)$. This is in general a rather strong simplification. Its variogram is given by 
$$\gamma_{sep}(h,u) = {\rm nug}\cdot {\bf1}_{h>0,u>0} + {\rm sill} \cdot \big( \gamma_s(h)+\gamma_t(u)-\gamma_s(h)\gamma_t(u) \big)$$
where $\gamma_s$ and $\gamma_t$ are spatial and temporal variograms without nugget effect and a sill of 1. The overall nugget and sill parameters are denoted by "$\rm nug$" and "$\rm sill$" respectively.
\end{frame}

\begin{frame}[fragile]
\frametitle{separable covariance model in R}
\footnotesize
\begin{verbatim}
separableModel <- vgmST("separable", 
                        space=vgm(0.9,"Exp", 147, 0.1),
                        time =vgm(0.9,"Exp", 3.5, 0.1),
                        sill=14)

modTempSep <- fit.StVariogram(vgmMeasure, separableModel,
                              lower=c(10,0,1,0,0))
attr(modTempSep, "optim.out")$value # Exp-Exp: 0.78

plot(vgmMeasure, modTempSep)

IstriaKrigeSep <- krigeST(TEMP~1, tempST[,110:120],
                             targetSTF,
                             model=modTempSep)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{variogram surface of the product-sum model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{sepStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 115 - seperable covariance model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{krigeSeparable.pdf}
\end{figure}
\end{frame}


\subsection{product-sum}

\begin{frame}
\frametitle{product-sum covariance model}
The \alert{product sum covariance model} extends the simplifying assumption of the separable covariance model to \cite{DeIaco2001}:
$$C_{ps}(h,u)=k_1 C_s(h) + k_2 C_t(u) + k_3 C_s(h)C_t(u)$$
with $k_1 >0$, $k_2 \geq 0$ and $k_3 \geq 0$ to fulfil the positive-definite condition. The corresponding variogram can be written as
$$ \gamma_{ps}(h,u) = {\rm nug}\cdot {\bf1}_{h>0,u>0} + \gamma_s(h)+\gamma_t(u) - k \gamma_s(h)\gamma_t(u) $$
where $\gamma_s$ and $\gamma_t$ are spatial and temporal variograms without nugget effect and in general different sills. The parameter $k$ needs to fulfil $0 < k \leq 1/(\max({\rm sill}_s,{\rm sill}_t))$ to let $\gamma_{ps}$ be a valid model. The overall nugget is denoted by "$\rm nug$".
\end{frame}

\begin{frame}[fragile]
\frametitle{product-sum covariance model in R}
\footnotesize
\begin{verbatim}
prodSumModel <- vgmST("productSum",
                      space=vgm(10, "Sph", 150, 0),
                      time= vgm(10, "Exp",   3, 0), 
                      sill=14, nugget=2)
modTempProdSum <- fit.StVariogram(vgmMeasure, prodSumModel,
                                  lower=c(0,10,0,1,10,0))
attr(modTempProdSum, "optim.out")$value # 0.71

plot(vgmMeasure, modTempProdSum)

IstriaKrigeProdSum <- krigeST(TEMP~1, tempST[,110:120],
                          targetSTF,
                          model=modTempProdSum)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{variogram of the product-sum model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{psStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 115 - product-sum covariance model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{krigeProdSum.pdf}
\end{figure}
\end{frame}


\subsection{sum-metric}

\begin{frame}
\frametitle{sum-metric covariance model}
The \alert{sum-metric covariance model} is given by:
$$C_{sm}(h,u)=C_s(h)+C_t(u)+C_j\big(\sqrt{h^2+(\kappa\cdot u)^2}\big)$$ 
Originally, this model allows for spatial, temporal and joint nugget effects, a simplified version may allow only for a joint nugget. The non-simplified variogram is given by
$$ \gamma_{sm}(h,u)=  \gamma_s(h) + \gamma_t(u) + \gamma_j(\sqrt{h^2+(\kappa\cdot u)^2})$$
where $\gamma_s$, $\gamma_t$ and $\gamma_j$ are spatial, temporal and joint variograms with a separate nugget-effect.
\end{frame}

\begin{frame}[fragile]
\frametitle{sum-metric covariance model in R}
\footnotesize
\begin{verbatim}
sumMetricModel <- vgmST("sumMetric",
                        space=vgm( 5, "Exp", 100, 1),
                        time =vgm( 5, "Mat",   5, 1, kappa=1),
                        joint=vgm( 5, "Exp", 100, 0),
                        stAni=20)
modTempSumMetric <- fit.StVariogram(vgmMeasure, sumMetricModel, 
                                    lower=c(0,10,0,
                                            0, 1,0,
                                            0,10,0,
                                            5))
attr(modTempSumMetric, "optim.out")$value # 0.44

plot(vgmMeasure, modTempSumMetric)

IstriaKrigeSumMetric <- krigeST(TEMP~1, tempST[,110:120],
                                targetSTF,
                                model=modTempSumMetric)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{variogram of the sum-metric model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{sumMetricStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 115 - sum-metric covariance model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{krigeSumMetric.pdf}
\end{figure}
\end{frame}

\begin{frame}{variogram of all spatio-temporal models}
\begin{figure}
\includegraphics[width=\textwidth]{allStVgm}
\end{figure}
\end{frame}

%\begin{frame}{kriged map for 10 days}
%\begin{figure}
%\includegraphics[width=\textwidth]{predSt}
%\end{figure}
%\end{frame}


\section{Local Spatio-Temporal Kriging}

\begin{frame}[fragile]
\frametitle{Local spatio-temporal kriging}
Purely spatial kriging allows to select the n-nearest neighbours and use only these for prediction.
\linebreak

What does \alert{nearest} mean in a spatio-temporal context?
\linebreak

The idea is to select the most \alert{valuable} locations, i.e. the strongest correlated ones.
\linebreak

Simply set the argument \verb?nmax? and a local neighbourhood of the most correlated values is selected from a larger "metric" neighbourhood.
\end{frame}

\begin{frame}[fragile]{Local spatio-temporal kriging in R}
\begin{verbatim}
krigeST(TEMP~1, tempST, nmax=10,
        targetSTFseries,
        model=projModTempSumMetric)
\end{verbatim}
\end{frame}

\section{spatio-temporal block kriging}
\begin{frame}
\frametitle{block kriging over time}
In the above scenario and with the presented methods, it is hard to get an uncertainty estimate of the temporally averaged value. Block kriging, with blocks over time, is one way to get such estimates. However, one has to decide on a model beforehand. Here, we will use the metric model again. \linebreak

Block kriging does not provide estimates for single locations but for areas or volumes. It has the property of providing the correct kriging variance for the block estimate that is typically lower due to the larger area. 
\end{frame}

\begin{frame}
\frametitle{monthly mean concentration - block kriged}
\begin{figure}
\includegraphics[scale=.6]{mean_concentration}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{block kriging variance}
\begin{figure}
\includegraphics[scale=.6]{block_variance}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriging variance day 15}
\begin{figure}
\includegraphics[scale=.6]{kriging_variance_day_15}
\end{figure}
\end{frame}

\begin{frame}[fragile]
\frametitle{block kriging in R - metric workaround}
\footnotesize
\begin{verbatim}
tmp_pred <- data.frame(cbind(ger_gridded@coords,15*tmpScale))
colnames(tmp_pred) <- c("x","y","t")
coordinates(tmp_pred) <- ~x+y+t
  
blockKrige <- krige(PM10~1, 
                    air3d[as.vector(!is.na(air3d@data)),],
                    newdata=tmp_pred, model=model3d, 
                    block=c(1,1,15*tmpScale))

ger_grid_time@sp@data <- blockKrige@data
\end{verbatim}
\end{frame}

\begin{frame}[allowframebreaks,fragile]{References}
\tiny
\bibliography{C:/Users/b_grae02/Documents/Forschung/commonBib}{}
\bibliographystyle{plain}
\end{frame}

\end{document}
